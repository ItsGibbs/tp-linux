# TP3 : Your own shiet - SACHA PERRIN

## Intro

Tout d'abord désolé pour le retard, j'espère néanmoins que ce TP sera à la hauteur de tes attentes.

Pour ce dernier TP j'ai décidé de monter un serveur VPN.

## Sommaire

- **Prérequis**
- **Manuel d'installation**
- **Setup monitoring**

## Prérequis

Afin de faire fonctionner ce service, vous aurez besoin de : 

- **Une machine Rocky Linux fonctionnelle.**

Vous pouvez télécharger l'ISO à l'adresse : https://rockylinux.org/download/

## Installation du VPN

**Pour commencer nous allons devoir installer certains paquet et définir certaines règles dans notre firewall**

```
[admin@vpn ~]$ sudo dnf update -y
[...]
Complete!
[admin@vpn ~]$ sudo dnf install epel-release -y
[...]
Complete!
[admin@vpn ~]$ sudo dnf install elrepo-release -y
[...]
Complete!
[admin@vpn ~]$ sudo dnf install kmod-wireguard wireguard-tools -y
[...]
Complete!
[admin@vpn ~]$ sudo firewall-cmd --remove-service dhcpv6-client --permanent
success
[admin@vpn ~]$ sudo firewall-cmd --remove-service cockpit --permanent
success
[admin@vpn ~]$ sudo firewall-cmd --reload
success
```

**Nous allons maintenant configurer wireguard**

```
[admin@vpn ~]$ sudo umask 077 | wg genkey | sudo tee /etc/wireguard/wireguard.key`
```
```
[admin@vpn /]# sudo wg pubkey < /etc/wireguard/wireguard.key | sudo tee /etc/wireguard/wireguard
```
```
[admin@vpn /]$ sudo nano /etc/wireguard/wg0.conf

[Interface]
Address = [Your VPN ip]
SaveConfig = true
ListenPort = 51820
DNS        = 8.8.8.8,1.1.1.1
PrivateKey = [Your private key]
PostUp = firewall-cmd --add-port=51820/udp; firewall-cmd --zone=public --add-masquerade; firewall-cmd --direct --add-rule ipv4 filter FORWARD 0 -i wg0 -o eth0 -j ACCEPT; firewall-cmd --direct --add-rule ipv4 nat POSTROUTING 0 -o eth0 -j MASQUERADE
PostDown = firewall-cmd --remove-port=51820/udp; firewall-cmd --zone=public --remove-masquerade; firewall-cmd --direct --remove-rule ipv4 filter FORWARD 0 -i wg0 -o eth0 -j ACCEPT; firewall-cmd --direct --remove-rule ipv4 nat POSTROUTING 0 -o eth0 -j MASQUERADE
```

**On active maintenant l'ip forwarding et on recharge la config systemctl**

```
[admin@vpn /]$ echo "net.ipv4.ip_forward = 1" | sudo tee -a /etc/sysctl.conf
net.ipv4.ip_forward = 1
[admin@vpn /]$ sudo sysctl -p
net.ipv4.ip_forward = 1
```

**Et maintenant on va pouvoir activer la carte réseau de notre VPN**

```
[admin@vpn ~]$ sudo systemctl start wg-quick@wg0
```

## Configuration du client

**Nous allons commencer par suivre le même processus de màj, installation, et gestion de règles que sur la machine VPN**

```
[admin@client ~]$ sudo dnf update -y
[...]
Complete!
[admin@client ~]$ sudo dnf install epel-release -y
[...]
Complete!
[admin@client ~]$ sudo dnf install elrepo-release -y
[...]
Complete!
[admin@client ~]$ sudo dnf install kmod-wireguard wireguard-tools -y
[...]
Complete!
[admin@client ~]$ sudo firewall-cmd --remove-service dhcpv6-client --permanent
success
[admin@client ~]$ sudo firewall-cmd --remove-service cockpit --permanent
success
[admin@client ~]$ sudo firewall-cmd --reload
success
```

**Générez maintenant une paire de clefs ssh**


```
[admin@client ~]$ sudo su -
[root@client ~]# wg genkey | tee /etc/wireguard/privatekey | wg pubkey | tee /etc/wireguard/publickey
[root@client ~]# exit
logout
```

**Modifiez le fichier de conf comme se suit :**

```
[admin@client ~]$ sudo nano /etc/wireguard/wg0.conf

[Interface]:
PrivateKey = [Client's private key]
Address = [Client's ip]

[Peer]
PublicKey = [VPN's public key]
Endpoint = [VPN's IP]:51820
AllowedIPs = 0.0.0.0/0
```

**Sur la machine VPN**

```
[admin@vpn ~]$ sudo wg set wg0 peer 69+c5x1jU89Q5RzK3J4Rl8q947KASY6c1MHpB1AT7Bc= allowed-ips 10.4.1.12
```

**Activez maintenant la carte réseau de votre client**

```
[admin@client ~]$ sudo systemctl start wg-quick@wg0
```

**Set le client en tant que pair du VPN**

Sur la machine VPN :

```
[admin@vpn ~]$ sudo wg set wg@0
```

## Préparation à la config NFS

**Sur le VPN :**

```
[admin@vpn ~]$ sudo dnf -y install nfs-utils
[...]
Complete!
```

```
[admin@vpn ~]$ sudo nano /etc/idmapd.conf
Domain = linux.tp3

[admin@vpn ~]$ sudo nano /etc/hosts
10.4.1.13 backup.linux.tp3 ##Remplacez par l'ip et le nom de votre machine backup
```

```
[admin@vpn ~]$ sudo nano /etc/exports
/srv/backup [Your VPN ip]/24(rw,no_root_squash)

[admin@vpn ~]$ sudo mkdir /backupfoldervpn
```
```
[admin@vpn ~]$ sudo mount -t nfs -o vers=3 [Your backup name]:/home/nfsshare /srv/backup/vpn/

[admin@vpn ~]$ sudo nano /etc/fstab
backup.linux.tp3:/home/nfsshare /srv/backup/vpn    nfs defaults    0 0
```

## Configuration de la machine backup

**Comme d'habitude, mise à jour et installation de package :**

```
[admin@backup ~]$ sudo dnf update -y
[...]
Complete!
[admin@backup ~]$ sudo dnf -y install nfs-utils
[...]
Complete!
[admin@backup ~]$ sudo firewall-cmd --remove-service dhcpv6-client --permanent
success
[admin@backup ~]$ sudo firewall-cmd --remove-service cockpit --permanent
success
[admin@backup ~]$ sudo firewall-cmd --reload
success
```

**Configurer le partage nfs**

```
[admin@backup ~]$ sudo nano /etc/idmap.conf
Domain = linux.tp3
[...]

[admin@backup ~]$ sudo nano /etc/exports
/srv/backup [Your backup ip]/24(rw,no_root_squash)

mkdir /backupfolder
```

**Mise en place d'une sauvegarde automatique sur la machine backup**

```
[admin@backup ~]$ sudo nano backup_tp3.sh

[admin@backup ~]$ sudo nano /etc/systemd/system/backup_tp3.service

[admin@backup ~]$ sudo systemctl daemon-reload
[admin@backup ~]$ sudo systemctl start backup_tp3.service

[admin@backup ~]$ sudo nano /etc/systemd/system/backup_tp3.timer

[admin@backup ~]$ sudo systemctl start backup_tp3.timer
```
###### *Tous les scripts sont disponibles sur le gitlab*

**Setup firewall**

```
[admin@backup ~]$ sudo firewall-cmd --add-service=nfs
[admin@backup ~]$ sudo firewall-cmd --add-service={nfs3,mountd,rpc-bind}
[admin@backup ~]$ sudo firewall-cmd --runtime-to-permanent
[admin@backup ~]$ sudo firewall-cmd --reload
```

## Setup monitoring netdata

**Installation et setup netdata**

Vous pouvez suivre ce processus pour n'importe quelle machine que vous souhaitez surveiller. Pour cet exemple, je serais sur ma machine VPN.

```
[admin@vpn ~]$ sudo su -
[root@vpn ~]$ bash <(curl -Ss https://my-netdata.io/kickstart-static64.sh) -y
exit
```

**Start et enable netdata**

```
[admin@vpn ~]$ sudo systemctl start netdata.service
[admin@vpn ~]$ sudo systemctl enable netdata.service
```

**Setup firewall**

```
[admin@vpn ~]$ sudo firewall-cmd --add-port=19999/tcp --permanent
[admin@vpn ~]$ sudo firewall-cmd --reload
```

**Config bot discord**

```
[admin@vpn ~]$ sudo nano /opt/netdata/etc/netdata/edit-config health_alarm_notify.conf
/DISCORD_WEBHOOK_URL
https://discord.com/api/webhooks/916364463257550878/LP5rY4alpDxceJHH_llExX40y5lb0dEqsWHYQofALjQKpXWem5mUfe6Fk0eQzWg_Cxl7
DEFAULT_RECIPIENT_DISCORD="alarms"
```

**Test alertes discord** 

```
[admin@vpn ~]$ sudo su -s /bin/bash netdata
[netdata@vpn ~]$ export NETDATA_ALARM_NOTIFY_DEBUG=1
/opt/netdata/usr/libexec/netdata/plugins.d/alarm-notify.sh test
[netdata@vpn ~]$ sudo sed -i 's/curl=""/curl="\/opt\/netdata\/bin\/curl -k"/' /opt/netdata/etc/netdata/health_alarm_notify.conf
```

**Config alertes RAM**

```
[admin@vpn ~]$ cd /opt/netdata/etc/netdata/
[admin@vpn ~]$ sudo ./edit-config health.d/ram.conf
/warn

warn: $this > (($status >= $WARNING)  ? (50) : (60))
crit: $this > (($status == $CRITICAL) ? (70) : (98))
```

**Stress test de la RAM pour vérifier le bon fonctionnement des alertes**
```
[admin@vpn ~]$ sudo dnf install stress -y
[admin@vpn ~]$ sudo stress --vm 4 --vm-bytes 1024M
```
