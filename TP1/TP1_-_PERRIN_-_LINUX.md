# TP1 - PERRIN - LINUX

# O. Préparation de la machine

#### 🌞 Setup de deux machines Rocky Linux configurées de façon basique.

En effectuant un ping vers les serveurs google (```8.8.8.8```), on peut s'assurer que les deux machines ont bien un accès internet via la carte NAT.

```
[admin@node1 /]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=114 time=19.1 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=114 time=20.2 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=114 time=19.5 ms
```
```
[admin@node2 /]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=114 time=19.3 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=114 time=22.7 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=114 time=20.4 ms
```

Les machines peuvent également se ```ping``` entre elles

```
[admin@node1 /]$ ping 10.101.1.12
PING 10.101.1.12 (10.101.1.12) 56(84) bytes of data.
64 bytes from 10.101.1.12: icmp_seq=1 ttl=64 time=0.499 ms
64 bytes from 10.101.1.12: icmp_seq=2 ttl=64 time=1.13 ms
64 bytes from 10.101.1.12: icmp_seq=3 ttl=64 time=0.569 ms
```
```
[admin@node2 /]$ ping 10.101.1.11
PING 10.101.1.11 (10.101.1.11) 56(84) bytes of data.
64 bytes from 10.101.1.11: icmp_seq=1 ttl=64 time=0.336 ms
64 bytes from 10.101.1.11: icmp_seq=2 ttl=64 time=0.550 ms
64 bytes from 10.101.1.11: icmp_seq=3 ttl=64 time=1.11 ms
```
Elles possèdent bien les noms requis
```
[admin@node1 /]$ hostname
node1.tp1.b2    
```
```
[admin@node2 /]$ hostname
node2.tp1.b2     
```
Elles utilisent bien ``1.1.1.1`` comme serveur DNS
```
[admin@node1 /]$ cat etc/sysconfig/network-scripts/ifcfg-enp0s8
TYPE=Ethernet
[...]
DNS1=1.1.1.1
```
```
[admin@node2 /]$ cat etc/sysconfig/network-scripts/ifcfg-enp0s8
TYPE=Ethernet
[...]
DNS1=1.1.1.1
```
Les machines peuvent communiquer entre elle en s'appelant par leurs noms

```
[admin@node1 /]$ ping node2.tp1.b2
PING node2 (10.101.1.12) 56(84) bytes of data.
64 bytes from node2 (10.101.1.12): icmp_seq=1 ttl=64 time=0.397 ms
64 bytes from node2 (10.101.1.12): icmp_seq=2 ttl=64 time=1.16 ms
64 bytes from node2 (10.101.1.12): icmp_seq=3 ttl=64 time=1.21 ms
```
Le Pare Feu est activé sur les deux machines
```
[admin@node1 /]$ sudo firewall-cmd --state
running
```
```
[admin@node2 /]$ sudo firewall-cmd --state
running
```
# I. Utilisateurs

## 1. Création et configuration

Nous allons commencer par créer un utilisateur ``user1`` sur notre ``node1`` 

```
sudo useradd user1 -d /home/user1 -s /bin/bash -u 100
```

```
[admin@node1 /]$ cat etc/passwd
root:x:0:0:root:/root:/bin/bash
[...]
admin:x:1000:1000:admin:/home/admin:/bin/bash
user1:x:100:1003::/home/user1:/bin/bash
```

Maintenant, il nous faut un groupe, ici ```admins```

```
sudo groupadd admins
```

```
cat etc/group
```

```
[admin@node1 /]$ cat etc/group
root:x:0:
[...]
admin:x:1000:
admins:x:1002:
```

Nous allons maintenant donner les permissions root au groupe admins en utilisant la commande ``visudo``

```
## Grant 'admins' group members root permission
%admins    ALL=(ALL)    ALL
"/etc/sudoers.tmp" 122L, 4373C written
```

Ajoutons ``user1`` au groupe ``admins``

```
sudo usermod -aG admins user1
```

Vérifions qu'il appartienne bien au groupe ``admins``

```
[admin@node1 /]$ groups user1
user1 : user1 admins
```

## 2. SSH

Je vais commencer par générer une paire de clef ssh grâce à la commande ``ssh-keygen -t rsa -b 4096``
```
PS C:\Users\thepa\Desktop\Cours\Linux\TP1> ssh-keygen -t rsa -b 4096
Generating public/private rsa key pair.
Enter file in which to save the key (C:\Users\thepa/.ssh/id_rsa): 
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in C:\Users\thepa/.ssh/id_rsa.
Your public key has been saved in C:\Users\thepa/.ssh/id_rsa.pub.
The key fingerprint is:
SHA256:q2m2XfWfRiYQhYSpvAWGOKFaSGN26YuM7vLXSrIMO1w thepa@LAPTOP-FLEOPDHT
The key's randomart image is:
+---[RSA 4096]----+
| = o+ .   +.o.   |
|+.++ . o o o     |
|.. .   o . .     |
|+ . .   S   o    |
|+ o ..  . .   =  |
|o* +. ++ .     o.|
|+++..++..     ...|
+----[SHA256]-----+     
```
Je vais maintenant utiliser ``scp`` pour transférer ma  clef publique de mon hôte à ma ``node1``
```
PS C:\Users\thepa\.ssh> scp .\id_rsa.pub user1@10.101.1.11:/home/user1/.ssh/
user1@10.101.1.11's password:
id_rsa.pub                                                                     100%  748   251.0KB/s   00:00
```
La clef est bien arrivée sur mon ``node1``, je n'ai plus qu'à copier le contenu dans ``authorized_keys``
```
[user1@node1 .ssh]$ ls
authorized_keys  id_rsa.pub
```
Je peux maintenant me connecter en SSH sans qu'un mot de passe me soit demandé
```
PS C:\Users\thepa\.ssh> ssh user1@10.101.1.11 -i C:\Users\thepa\.ssh\id_rsa
user1@10.101.1.11's password:
Activate the web console with: systemctl enable --now cockpit.socket

Last login: Tue Sep 28 11:58:20 2021 from 10.101.1.1
[user1@node1 .ssh]$
```

# II. Partitionnement

## 1. Préparation de la VM

Pour préparer la VM, il m'a suffit d'ajouter deux disques de 3Go chacun à ma VM Node1

## 2. Partitionnement

Pour commencer je vais utiliser ```lsblk``` pour lister mes espaces de stockage : 

![](https://i.imgur.com/hZDfcCW.png)

Je vais maintenant les ajouter en tant que PV avec ```sudo pvcreate /dev/sdb``` puis ```sudo pvcreate /dev/sdc```

![](https://i.imgur.com/KCalcT6.png)

Ensuite, je vais créer mon groupe et y ajouter mes deux PV : 

![](https://i.imgur.com/7sRXtCw.png)

Maintenant, je vais créer trois LV de 1Go chacun

Je vais utiliser ```sudo lvcreate -L 1G tp -n tp1``` : 

![](https://i.imgur.com/anqdlDW.png)

Par la suite je vais les formater en ```ext4``` en employant la commande ```sudo mkfs -t ext4 /dev/tp/tp1``` : 

![](https://i.imgur.com/MmES1sF.png)

Pour pouvoir les monter à un point de montage, je vais commencer par les créer : 

![](https://i.imgur.com/mS5yNBW.png)

Maintenant je vais pouvoir utiliser ```sudo mount /dev/tp/tp1 /mnt/part1``` pour les monter : 

![](https://i.imgur.com/w2eDtW6.png)

Afin de vérifier que tout soit en ordre, j'utilise la commande ```mount``` suivi de ```df -h``` : 

![](https://i.imgur.com/ErG3xIm.png)

Je vais maintenant modifier ```etc/fstab``` afin que les partitions soient lancées automatiquement au démarrage du système.

![](https://i.imgur.com/V7YvbgG.png)

# III. Gestion de services

## 1. Interaction avec un service existant

Pour s'assurer que ```firewalld``` soit démarrée et activée on va utiliser ``is-active`` et ``is-enabled`` avec ``systemctl``
```
[user1@node1 /]$ systemctl is-active firewalld
active
[user1@node1 /]$ systemctl is-enabled firewalld
enabled
```
## 2. Création de service

### A. Unité simpliste

#### 🌞 Créer un fichier qui définit une unité de service ``web.service`` dans le répertoire ``/etc/systemd/system``.
On va ouvrir le port ``8888`` sur le firewall pour pouvoir y lancer le serveur web
```
[user1@node1 /]$ sudo firewall-cmd --add-port=8888/tcp
success
[user1@node1 /]$ sudo firewall-cmd --reload
success
```
On va ensuite créer notre ``web.service`` dans le répertoire ``/etc/systemd/system``
```
[user1@node1 /]$ cat etc/systemd/system/web.service
[Unit]
Description=Very simple web service

[Service]
ExecStart=/bin/python3 -m http.server 8888

[Install]
WantedBy=multi-user.target
```
Pour qu'il puisse fonctionner on va rapidement installer ``python3`` avec la commande ``dnf install python3``

On va maintenant demander à ``systemd`` de relire les fichiers de configuration
```
[user1@node1 /]$ sudo systemctl daemon-reload
```
Maintenant on va pouvoir démarer notre unité
```
[user1@node1 /]$ sudo systemctl start web
```
Et vérifier son status
```
[user1@node1 /]$ sudo systemctl status web
● web.service - Very simple web service
   Loaded: loaded (/etc/systemd/system/web.service; enabled; vendor preset: disabled)
   Active: active (running) since Tue 2021-09-28 14:25:50 CEST; 2s ago
 Main PID: 25249 (python3)
    Tasks: 1 (limit: 4946)
   Memory: 9.6M
   CGroup: /system.slice/web.service
           └─25249 /bin/python3 -m http.server 8888

Sep 28 14:25:50 node1.tp1.b2 systemd[1]: Started Very simple web service.
```
Mais est-ce que ça fonctionne ?
```
[user1@node1 /]$ curl 10.101.1.11:8888
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Directory listing for /</title>
</head>
<body>
<h1>Directory listing for /</h1>
<hr>
<ul>
<li><a href="bin/">bin@</a></li>
<li><a href="boot/">boot/</a></li>
<li><a href="dev/">dev/</a></li>
<li><a href="etc/">etc/</a></li>
<li><a href="home/">home/</a></li>
<li><a href="lib/">lib@</a></li>
<li><a href="lib64/">lib64@</a></li>
<li><a href="media/">media/</a></li>
<li><a href="mnt/">mnt/</a></li>
<li><a href="opt/">opt/</a></li>
<li><a href="proc/">proc/</a></li>
<li><a href="root/">root/</a></li>
<li><a href="run/">run/</a></li>
<li><a href="sbin/">sbin@</a></li>
<li><a href="srv/">srv/</a></li>
<li><a href="sys/">sys/</a></li>
<li><a href="tmp/">tmp/</a></li>
<li><a href="usr/">usr/</a></li>
<li><a href="var/">var/</a></li>
</ul>
<hr>
</body>
</html>
```
Ca fonctionne ! :D

![Alt Text](https://media.giphy.com/media/SRO0ZwmImic0/giphy.gif?cid=ecf05e47o2rcyqghctqh58pqnqsyr3hpt21kke3fqopjpp4q&rid=giphy.gif&ct=g)

### B. Modification de l'unité

#### 🌞 Créer un utilisateur ``web``.

On va donc créer un utilisateur ``web`` avec ``sudo useradd web`` et lui donner les droits d'admins

```
[user1@node1 /]$ sudo usermod -aG admins web
```

#### 🌞 Modifiez l'unité de service web.service créée précédemment en ajoutant les clauses 

```
[web@node1 /]$ sudo cat etc/systemd/system/web.service
[Unit]
Description=Very simple web service

[Service]
ExecStart=/bin/python3 -m http.server 8888
User=web
WorkingDirectory=/srv/webserver

[Install]
WantedBy=multi-user.target
```

#### 🌞 Placer un fichier de votre choix dans le dossier créé dans ``/srv`` et tester que vous pouvez y accéder une fois le service actif.

```
[web@node1 /]$ sudo touch srv/webserver/web.text
```
#### 🌞 Vérifier le bon fonctionnement avec une commande ``curl``
```
[user1@node1 webserver]$ curl 10.101.1.11:8888/srv/
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Directory listing for /srv/</title>
</head>
<body>
<h1>Directory listing for /srv/</h1>
<hr>
<ul>
<li><a href="webserver/">webserver/</a></li>
</ul>
<hr>
</body>
</html>
```

#### Tout fonctionne !
##### Allez, un dernier gif de chat qui fait la fête pour cloturer ce TP

![Alt Text](https://media.giphy.com/media/QDRJ6IJzFSR1K/giphy-downsized-large.gif?cid=ecf05e47jhwt7fzh8tb0bomsy120dnmtojf74pwgwenkzsy7&rid=giphy-downsized-large.gif&ct=g)