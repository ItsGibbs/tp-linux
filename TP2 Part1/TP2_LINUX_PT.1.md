# TP2 LINUX PT.1

# I. Un premier serveur web

## 1. Installation

#### 🌞 Installer le serveur Apache

```
[admin@web /]$ ls etc/httpd/
conf  conf.d  conf.modules.d  logs  modules  run  state
```

#### 🌞 Démarrer le service Apache

```
[admin@web ~]$ systemctl enable httpd.service

[admin@web ~]$ systemctl start httpd.service

[admin@web ~]$ systemctl list-unit-files --type service
[...]
httpd.service                              enabled

[admin@web ~]$ systemctl status httpd.service
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor prese>
   Active: active (running) since Wed 2021-09-29 17:50:35 CEST; 2min 24s ago

[admin@web ~]$ firewall-cmd --add-port=80/tcp

[admin@web /]$ ss -alnpt
State     Recv-Q    Send-Q       Local Address:Port       Peer Address:Port    Process
[...]
LISTEN    0         128                      *:80                    *:*        users:(("httpd",pid=1693,fd=4),("httpd",pid=1692,fd=4),("httpd",pid=1691,fd=4),("httpd",pid=1689,fd=4))
```

#### 🌞 TEST

```
[admin@web /]$ systemctl status httpd
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
  Drop-In: /usr/lib/systemd/system/httpd.service.d
           └─php74-php-fpm.conf
   Active: active (running) since Sun 2021-10-10 20:46:49 CEST; 5h 27min ago
     Docs: man:httpd.service(8)
  Process: 8287 ExecReload=/usr/sbin/httpd $OPTIONS -k graceful (code=exited, status=0/SUCCESS)
 Main PID: 11610 (httpd)
   Status: "Total requests: 323; Idle/Busy workers 100/0;Requests/sec: 0.0164; Bytes served/sec: 1.5KB/sec"
    Tasks: 278 (limit: 4946)
   Memory: 41.9M
   CGroup: /system.slice/httpd.service
           ├─11610 /usr/sbin/httpd -DFOREGROUND
           ├─11612 /usr/sbin/httpd -DFOREGROUND
           ├─11613 /usr/sbin/httpd -DFOREGROUND
           ├─11614 /usr/sbin/httpd -DFOREGROUND
           ├─11615 /usr/sbin/httpd -DFOREGROUND
           └─14751 /usr/sbin/httpd -DFOREGROUND

Oct 10 20:46:49 web.tp2.linux systemd[1]: httpd.service: Succeeded.
Oct 10 20:46:49 web.tp2.linux systemd[1]: Stopped The Apache HTTP Server.
Oct 10 20:46:49 web.tp2.linux systemd[1]: Starting The Apache HTTP Server...
Oct 10 20:46:49 web.tp2.linux systemd[1]: Started The Apache HTTP Server.
Oct 10 20:46:49 web.tp2.linux httpd[11610]: Server configured, listening on: port 80
```

## 2. Avancer vers la maîtrise du service

#### 🌞 Le service Apache...

```
[admin@web /]$ cat etc/systemd/system/multi-user.target.wants/httpd.service
[...]
[Unit]
Description=The Apache HTTP Server
Wants=httpd-init.service
After=network.target remote-fs.target nss-lookup.target httpd-init.service
Documentation=man:httpd.service(8)
[...]
```

#### 🌞 Déterminer sous quel utilisateur tourne le processus Apache

```
[admin@web /]$ cat etc/httpd/conf/httpd.conf

ServerRoot "/etc/httpd"

Listen 80

Include conf.modules.d/*.conf

User apache
Group apache


ServerAdmin root@localhost
[...]
```

```
[admin@web ~]$ ps -ef | grep httpd
root       11610       1  0 Oct10 ?        00:00:01 /usr/sbin/httpd -DFOREGROUND
apache     11612   11610  0 Oct10 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache     11613   11610  0 Oct10 ?        00:00:07 /usr/sbin/httpd -DFOREGROUND
apache     11614   11610  0 Oct10 ?        00:00:06 /usr/sbin/httpd -DFOREGROUND
apache     11615   11610  0 Oct10 ?        00:00:06 /usr/sbin/httpd -DFOREGROUND
apache     14751   11610  0 02:05 ?        00:00:01 /usr/sbin/httpd -DFOREGROUND
```

```
[admin@web /]$ ls -al usr/share/testpage/
total 12
drwxr-xr-x.  2 root root   24 Oct 10 11:25 .
drwxr-xr-x. 91 root root 4096 Oct 10 11:25 ..
-rw-r--r--.  1 root root 7621 Jun 11 17:23 index.html
```

#### 🌞 Changer l'utilisateur utilisé par Apache

```
[admin@web /]$ useradd -r -s /sbin/nologin Comanche
```

```
[admin@web /]$ sudo cat etc/httpd/conf/httpd.conf

ServerRoot "/etc/httpd"

Listen 80

Include conf.modules.d/*.conf

User Comanche
Group Comanche


ServerAdmin root@localhost
[...]
```

```
[admin@web /]$ systemctl restart httpd
```

```
[admin@web /]$ ps -ef | grep httpd
root        2376       1  0 14:52 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
Comanche    2378    2376  0 14:52 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
Comanche    2379    2376  0 14:52 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
Comanche    2380    2376  0 14:52 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
Comanche    2381    2376  0 14:52 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
admin       2595    1449  0 14:53 pts/0    00:00:00 grep --color=auto httpd
```

#### 🌞 Faites en sorte que Apache tourne sur un autre port

```
[admin@web /]$ sudo cat etc/httpd/conf/httpd.conf

ServerRoot "/etc/httpd"

Listen 667
[...]
```
```
[admin@web /]$ sudo firewall-cmd --add-port=667/tcp
success
```
```
[admin@web /]$ sudo firewall-cmd --remove-port=80/tcp
success
```

```
[admin@web /]$ sudo ss -lanp
[...]
tcp   LISTEN 0      128                                                        *:667                     *:*
 users:(("httpd",pid=3160,fd=9),("httpd",pid=2939,fd=9),("httpd",pid=2938,fd=9),("httpd",pid=2937,fd=9),("httpd",pid=2376,fd=9))
```
```
[admin@web /]$ curl localhost:667
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
      /*<![CDATA[*/
[...]
```
# II. Une stack web plus avancée

## 1. Intro
```
[admin@web /]$ cat etc/httpd/conf/httpd.conf
[...]
IncludeOptional sites-enabled/*
```
## 2. Setup

### A. Serveur Web et NextCloud

#### 🌞 Install du serveur Web et de NextCloud sur web.tp2.linux

```
sudo dnf install epel-release
sudo dnf update
sudo dnf install https://rpms.remirepo.net/enterprise/remi-release-8.rpm
sudo dnf module enable php:remi-7.4
sudo dnf install httpd vim wget zip unzip libxml2 openssl php74-php php74-php-ctype php74-php-curl php74-php-gd php74-php-iconv php74-php-json php74-php-libxml php74-php-mbstring php74-php-openssl php74-php-posix php74-php-session php74-php-xml php74-php-zip php74-php-zlib php74-php-pdo php74-php-mysqlnd php74-php-intl php74-php-bcmath php74-php-gmp
sudo mkdir /etc/httpd/sites-available
sudo vi /etc/httpd/sites-available/tp2.linux.nextcloud
sudo mkdir /etc/httpd/sites-enabled
sudo ln -s /etc/httpd/sites-available/tp2.linux.nextcloud /etc/httpd/sites-enabled/
timedatectl
sudo vim /etc/opt/remi/php74/php.ini
wget https://download.nextcloud.com/server/releases/nextcloud-22.2.0.zip
unzip nextcloud-22.2.0.zip
cd nextcloud/
sudo cp -Rf * /var/www/sub-domains/tp2.linux.nextcloud/html/
sudo chown -Rf apache.apache /var/www/sub-domains/tp2.linux.nextcloud
sudo systemctl restart httpd
```

### B. Base de données

#### 🌞 Install de MariaDB sur db.tp2.linux

```
[admin@db ~]$ ss -alpnt
State        Recv-Q       Send-Q             Local Address:Port             Peer Address:Port      Process
LISTEN       0            128                      0.0.0.0:22                    0.0.0.0:*
LISTEN       0            80                             *:3306                        *:*
LISTEN       0            128                         [::]:22                       [::]:*
```

#### 🌞 Préparation de la base pour NextCloud

```
MariaDB [(none)]> CREATE USER 'nextcloud'@'10.102.1.11' IDENTIFIED BY 'meow';
Query OK, 0 rows affected (0.000 sec)

MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
Query OK, 1 row affected (0.000 sec)

MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.102.1.11';
Query OK, 0 rows affected (0.000 sec)

MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.000 sec)
```

#### 🌞 Exploration de la base de données

```
MariaDB [(none)]> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| nextcloud          |
+--------------------+
2 rows in set (0.002 sec)

MariaDB [(none)]> USE nextcloud;
Database changed
MariaDB [nextcloud]> SHOW TABLES;
Empty set (0.000 sec)
```

```
MariaDB [(none)]> SELECT User, Host FROM mysql.user;
+-----------+-------------+
| User      | Host        |
+-----------+-------------+
| nextcloud | 10.102.1.11 |
| root      | 127.0.0.1   |
| root      | ::1         |
| root      | localhost   |
+-----------+-------------+
4 rows in set (0.000 sec)
```

### C. Finaliser l'installation de NextCloud

#### 🌞 Sur votre PC

- Dans ``C:/Windows/System32/drivers/etc``, fichier ``hosts`` :
```
10.102.1.11 web web.tp2.linux
```

#### 🌞 Exploration de la base de données

```
MariaDB [(none)]> SELECT count(*) AS TOTALNUMBEROFTABLES FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'nextcloud';
+---------------------+
| TOTALNUMBEROFTABLES |
+---------------------+
|                 108 |
+---------------------+
1 row in set (0.001 sec)

```


| Machine         | IP            | Service                 | Port ouvert | IP autorisées |
|-----------------|---------------|-------------------------|-------------|---------------|
| `web.tp2.linux` | `10.102.1.11` | Serveur Web             | 80/tcp    | ``All``             |
| `db.tp2.linux`  | `10.102.1.12` | MariaDB                 | 3306/tcp      | ``10.102.1.11``           |