# TP2 LINUX PT.2

# I. Monitoring

## 1. Le concept

## 2. Setup

#### 🌞 Setup Netdata

- Intallation de Netdata sur le serveur web et la database

```
[admin@web /]$ sudo su -

[root@web ~]# bash <(curl -Ss https://my-netdata.io/kickstart-static64.sh)
```

```
^
|.-.   .-.   .-.   .-.   .-.   .  netdata              .-.   .-.   .-.   .-
|   '-'   '-'   '-'   '-'   '-'   is installed now!  -'   '-'   '-'   '-'
+----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+--->
```

```
[root@web ~]# exit
logout
```

#### 🌞 Manipulation du service Netdata

```
[admin@web ~]$ systemctl enable netdata

[admin@web ~]$ systemctl start netdata

[admin@web ~]$ systemctl list-unit-files --type service
[...]
netdata.service                              enabled

[admin@web /]$ systemctl status netdata
● netdata.service - Real time performance monitoring
   Loaded: loaded (/usr/lib/systemd/system/netdata.service; enabled; vendor preset: disabled)
   Active: active (running) since Thu 2021-10-21 09:49:38 CEST; 16min ago
[...]
```

```
[admin@web /]$ ss -alpnt
State        Recv-Q       Send-Q             Local Address:Port              Peer Address:Port      Process
[...]
LISTEN       0            128                         [::]:19999                     [::]:*
```

```
[admin@web /]$ sudo firewall-cmd --add-port=19999/tcp
success
```

#### 🌞 Setup Alerting

```
###############################################################################
# sending discord notifications

# note: multiple recipients can be given like this:
#                  "CHANNEL1 CHANNEL2 ..."

# enable/disable sending discord notifications
SEND_DISCORD="YES"

# Create a webhook by following the official documentation -
# https://support.discordapp.com/hc/en-us/articles/228383668-Intro-to-Webhooks
DISCORD_WEBHOOK_URL="https://discord.com/api/webhooks/900678341835776070/cFweJklBafz6I43RWZkY12IE42DQA08kMnOV1MTdUcCNzEEezzIoytDreAuX19ntzlHi"

# if a role's recipients are not configured, a notification will be send to
# this discord channel (empty = do not send a notification for unconfigured
# roles):
DEFAULT_RECIPIENT_DISCORD="netdata-web"
```

![](https://i.imgur.com/gFKf8lL.png)

```
[admin@web netdata]$ sudo sed -i 's/curl=""/curl="\/opt\/netdata\/bin\/curl -k"/' /opt/netdata/etc/netdata/health_alarm_notify.conf
```

#### 🌞 Config alerting

On va créer ``ram-usage.conf`` dans ``netdata/health.d/`` et le remplir avec :

```
alarm: ram_usage
on: system.ram
lookup: average -1m percentage of used
units: %
every: 1m
warn: $this > 50
crit: $this > 90
info: The percentage of RAM being used by the system.
```

On va maintenant installer le package ``stress`` avec ``sudo dnf install stress`` puis lancer les commandes suivantes : 

```
[admin@web health.d]$ sudo killall -USR2 netdata
[admin@web health.d]$ stress --vm-bytes $(awk '/MemAvailable/{printf "%d\n", $2 * 0.98;}' < /proc/meminfo)k --vm-keep -m 1
stress: info: [27330] dispatching hogs: 0 cpu, 0 io, 1 vm, 0 hdd
```

Maintenant que le stress test est lancé, je reçois bien mes alertes sur discord :

![](https://i.imgur.com/Yyf1X4Q.png)

# II. Backup

## 1. Intwo bwo

## 2. Partage NFS

#### 🌞 Setup environnement

``mkdir /srv/backups`` 

``mkdir /srv/backups/web.tp2.linux``

#### 🌞 Setup partage NFS

```
[admin@backup /]$ sudo dnf -y install nfs-utils
[...]
Installed:
  gssproxy-0.8.0-19.el8.x86_64        keyutils-1.5.10-6.el8.x86_64        libevent-2.1.8-5.el8.x86_64        libverto-libevent-0.3.0-5.el8.x86_64        nfs-utils-1:2.3.3-41.el8_4.2.x86_64        rpcbind-1.2.5-8.el8.x86_64

Complete!
```

```
[admin@backup /]$ sudo nano etc/idmapd.conf

Domain=tp2.linux

[admin@backup /]$ sudo nano etc/exports

#Create new
/srv/backups/web.tp2.linux 10.102.1.0/24(rw,no_root_squash)
/srv/backups/db.tp2.linux 10.102.1.0/24(rw,no_root_squash)

[admin@backup /]$ mkdir /home/nfsshare
mkdir /home/nfsshare
[admin@backup /]$ sudo systemctl enable --now rpcbind nfs-server
Created symlink /etc/systemd/system/multi-user.target.wants/nfs-server.service → /usr/lib/systemd/system/nfs-server.service.
[admin@backup /]$ sudo firewall-cmd --add-service=nfs
success
[admin@backup /]$ sudo firewall-cmd --runtime-to-permanent
success
```

#### 🌞 Setup points de montage sur web.tp2.linux

```
[admin@web /]$ sudo mount -t nfs 10.102.1.13:/srv/backups/web.tp2.linux /srv/backup/
```

```
[admin@web /]$ sudo mount -l | grep backups
10.102.1.13:/srv/backups/web.tp2.linux on /srv/backup type nfs4 (rw,relatime,vers=4.2,rsize=131072,wsize=131072,namlen=255,hard,proto=tcp,timeo=600,retrans=2,sec=sys,clientaddr=10.102.1.11,local_lock=none,addr=10.102.1.13)   
```

```
[admin@web /]$ sudo df -hT
Filesystem                             Type      Size  Used Avail Use% Mounted on
devtmpfs                               devtmpfs  387M     0  387M   0% /dev
tmpfs                                  tmpfs     405M  352K  405M   1% /dev/shm
tmpfs                                  tmpfs     405M  5.6M  400M   2% /run
tmpfs                                  tmpfs     405M     0  405M   0% /sys/fs/cgroup
/dev/mapper/rl-root                    xfs        47G  4.2G   43G   9% /
/dev/sda1                              xfs      1014M  303M  712M  30% /boot
tmpfs                                  tmpfs      81M     0   81M   0% /run/user/1000
10.102.1.13:/srv/backups/web.tp2.linux nfs4       47G  2.5G   45G   6% /srv/backup
```

```
[admin@web /]$ cd /srv/backup/
[admin@web backup]$ sudo touch coucou
[admin@web backup]$ ls
coucou
```

```
[admin@web /]$ sudo nano etc/fstab
backup.tp2.linux:/srv/backups/web.tp2.linux /srv/backup          nfs     defaults        0 0
```

#### 🌟 BONUS : partitionnement avec LVM



## 3. Backup de fichiers

#### 🌞 Rédiger le script de backup /srv/tp2_backup.sh

##### 📁 Fichier ``/srv/tp2_backup.sh``

#### 🌞 Tester le bon fonctionnement

```
[admin@backup srv]$ ls
backups  coucou  scriptsback  selem  tp2_backup.sh
[admin@backup srv]$ sudo ./tp2_backup.sh backups/ selem/
tar: Removing leading `/' from member names
/srv/selem/
/srv/selem/test
sending incremental file list
tp2_backup_211028_100531.tar.gz

sent 260 bytes  received 43 bytes  606.00 bytes/sec
total size is 144  speedup is 0.48
/srv
[admin@backup srv]$ ls backups/
db.tp2.linux  tp2_backup_211028_100531.tar.gz  web.tp2.linux
[admin@backup srv]$ sudo tar xzvf backups/tp2_backup_211028_100531.tar.gz
srv/selem/
srv/selem/test
```


## 4. Unité de service

### A. Unité de service

#### 🌞 Créer une unité de service

```
[admin@backup /]$ cd /etc/systemd/system/
[admin@backup system]$ sudo nano tp2_backup.service
```

```
[Unit]
Description=Our own lil backup service (TP2)

[Service]
ExecStart=/srv/tp2_backup.sh /srv/dir1 /srv/dir2
Type=oneshot
RemainAfterExit=no

[Install]
WantedBy=multi-user.target

```

#### 🌞 Tester le bon fonctionnement

```
[admin@backup srv]$ sudo systemctl daemon-reload
[admin@backup srv]$ sudo systemctl start tp2_backup
[admin@backup srv]$ ls
backups  coucou  dir1  dir2  scriptsback  selem  srv  tp2_backup.sh
[admin@backup srv]$ ls dir1
tp2_backup_211028_121844.tar.gz
[admin@backup srv]$
```

### B. Timer

#### 🌞 Créer le timer associé à notre tp2_backup.service

```
[admin@backup srv]$ sudo nano /etc/systemd/system/tp2_backup.timer
```

#### 🌞 Activez le timer

```
[admin@backup srv]$ sudo systemctl daemon-reload
[admin@backup srv]$ sudo systemctl start tp2_backup.timer
[admin@backup srv]$ sudo systemctl enable tp2_backup.timer
[admin@backup srv]$ sudo systemctl is-active tp2_backup.timer
active
[admin@backup srv]$ sudo systemctl is-enabled tp2_backup.timer
enabled
```

#### 🌞 Tests !

``` 
[admin@backup srv]$ date
Thu Oct 28 12:37:59 CEST 2021
[admin@backup srv]$ ls dir1
tp2_backup_211028_123307.tar.gz  tp2_backup_211028_123507.tar.gz  tp2_backup_211028_123707.tar.gz
tp2_backup_211028_123407.tar.gz  tp2_backup_211028_123607.tar.gz
```

### C. Contexte

#### 🌞 Faites en sorte que...


-

##### 📁 Fichier ``/etc/systemd/system/tp2_backup.timer``
##### 📁 Fichier ``/etc/systemd/system/tp2_backup.service``

## 5. Backup de base de données

## 6. Petit point sur la backup

# III. Reverse Proxy

## 1. Introooooo

## 2. Setup simple

## 3. Bonus HTTPS

# IV. Firewalling

## 1. Présentation de la syntaxe

## 2. Mise en place

### A. Base de données

### B. Serveur Web

### C. Serveur de backup

### D. Reverse Proxy

### E. Tableau récap

