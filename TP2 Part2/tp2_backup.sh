#!/bin/bash

# script Backup
# Sacha 28/10/21

# Fixe date
day=$(date +%y%m%d_%H%M%S)

# Backup name
backups=tp2_backup_$day.tar.gz

# Absolute path
source_file=$(readlink -f $2)
destination=$(readlink -f $1)
archive=$(readlink -f $backups)

# Compress directory
tar -czvf $backups $source_file

# Send backup
rsync -av --remove-source-files  $archive $destination;

# Delete more than 5 backup files
cd $destination
ls -tQ  | tail -n+6 | xargs rm &>/dev/null;
cd -

